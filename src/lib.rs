#![allow(unused)]
pub mod argument {
    use structopt::StructOpt;

    #[derive(Debug, StructOpt)]
    #[structopt(name = "rust_ed", about = "A Rust implementation of GNU ed.")]
    pub struct Arg {
        /// Check version and license info
        #[structopt(short = "V", long = "version")]
        pub version: bool,

        /// Quiet output (without byte counts)
        #[structopt(short, long)]
        pub quiet: bool,

        // be verbose as for byte counts, etc.
        /// Verbosity (-v, -vv , -vvv, etc.)
        #[structopt(short, long, parse(from_occurrences))]
        pub verbosity: u8,

        /// Path of file to edit
        #[structopt(parse(from_os_str))]
        pub path: std::path::PathBuf,
    }
}

pub mod parser {
    use std::mem;

    #[derive(Debug)]

    pub enum TokenKind {
        LChr,
        UChr,
        Symb,
        Num,
    }

    pub struct Token {
        kind: TokenKind,
        value: String,
    }

    impl Command {
        pub fn new() -> Self {
            let mut cmd = String::new();

            std::io::stdin().read_line(&mut cmd);
            rm_whitespace(&cmd);

            Self { cmd }
        }

        pub fn from(pattern: String) -> Self {
            rm_whitespace(&pattern);
            Self { cmd: pattern }
        }

        pub fn tokenize(&self) -> Vec<Token> {
            let mut tokens: Vec<Token> = Vec::new();
            let mut num_buf = String::new();
            let mut last_char_num: bool = false;

            for character in self.cmd.chars() {
                if character.is_ascii_digit() {
                    num_buf.push(character);
                    last_char_num = true;

                    continue;
                }

                if !(num_buf.is_empty()) {
                    tokens.push(Token {
                        kind: TokenKind::Num,
                        value: num_buf.clone(),
                    });
                    num_buf.clear();
                    last_char_num = false;
                }

                if character.is_ascii_uppercase() {
                    tokens.push(Token {
                        kind: TokenKind::UChr,
                        value: character.to_string(),
                    });
                }
                if character.is_ascii_lowercase() {
                    tokens.push(Token {
                        kind: TokenKind::LChr,
                        value: character.to_string(),
                    });
                }
                if character.is_ascii_punctuation() {
                    tokens.push(Token {
                        kind: TokenKind::Symb,
                        value: character.to_string(),
                    });
                }
            }

            tokens
        }
    }

    pub fn parse(arr: Vec<Token>) -> () {}

    impl PartialEq for Token {
        fn eq(&self, other: &Self) -> bool {
            mem::discriminant(&self.kind) == mem::discriminant(&other.kind)
                && &self.value == &other.value
        }
    }

    fn rm_whitespace(s: &str) -> String {
        s.chars().filter(|c| !c.is_whitespace()).collect()
    }
}
