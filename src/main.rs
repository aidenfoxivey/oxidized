/*
rust'ed -- a rust implementation of the classic GNU ed
Copyright (C) 2021 Aiden Fox Ivey

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use rust_ed::{argument, parser};
use std::{env, error::Error, fs, io, path, process};
use structopt::{clap::crate_version, StructOpt};


static PROG_YEAR: u32 = 2021;
static PROG_NAME: &str = "rust_ed";
static PROG_VERS: &str = crate_version!();

fn main() {
    let args = argument::Arg::from_args();
    let result = fs::read_to_string(&args.path);

    run(args, result);
}

fn run(args: argument::Arg, result: Result<String, io::Error>) -> () {
    if args.version == true {
        show_version();
        process::exit(0);
    }

    let mut content = match result {
        Ok(content) => content,
        Err(error) => {
            eprintln!("{} given input {:#?}.", error, args.path);
            process::exit(1);
        }
    };
}

fn show_version() -> () {
    println!(
        "{} ver. {}    Copyright (C) {} Aiden Fox Ivey",
        PROG_NAME, PROG_VERS, PROG_YEAR
    );
    println!("  This is free software, and you are welcome to change and redistribute it.");
    println!("  Note that there is ABSOLUTELY NO WARRANTY, to the extent permitted by law.");
}

fn read_command() -> Result<String, Err> {
        
}

fn exec_command(cmd: &String) -> Result<(), Err> {
    let chars: Vec<char> = cmd.chars().collect(); 

    let ch: char = cmd.next();
    let is_num: bool = (ch in "1234567890");

    if (is_num) {
         
    } else {
        match ch {
            'a' => {
                append_text(start_index);
            },
            'c' => {
                change_lines(start_index, end_index);
            },
            'd' => {
                delete_lines(start_index, end_index);
            },
            'e' => {
                edit_file(false);
            },
            'E' => {
                edit_file(true);
            },
            'f' => {
                set_filename();
            },
            'g' => {},   
            'G' => {},
            'h' => {},
            'H' => {},
            'i' => {},
            'j' => {},
            'k' => {},
            'l' => {},
            'm' => {},
            'n' => {},
            'p' => {},
            'P' => {},
            'q' => {},
            'Q' => {},
            'r' => {},
            't' => {}, 
            'u' => {},
            'v' => {},
            'w' => {},
            'W' => {},
            'x' => {},
            'y' => {},
            'z' => {},
        }
    }
}

fn read_file(path: &String) -> Result<u8, Err> {
    let path: bool = Path::new(path).unwrap();

    let is_file: bool = Path::new(path).is_file();
    let is_directory: bool = Path::new().is_dir();
    // check if the path is to a file 

    if (!path.is_file() || path.is_dir()) {
        return (Err)
    }
    
    let file = File::open(path).unwrap("Unable to read the file");

    file
}
