use regex::Regex;

enum Operation {
    Replace,
    Match, 
    Print,
}

fn handle_regex(full_text: &str, reg_str: &str, operation: Operation) {
    reg_str = reg_str[1..-2]; 

    let re = Regex::new(r"{}", reg_str);

    match operation {
        Replace => {

        },
        Match => {

        },
        Print => {

        },
    }
}
